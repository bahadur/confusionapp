angular.module('conFusion.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };


  $ionicModal.fromTemplateUrl('templates/reserve.html', {
    scope: $scope
  }).then(function(modal){
    $scope.reserveForm = modal;
  });

  $scope.closeReserve = function(){
    $scope.reserveForm.hide();
  };

  $scope.reserve = function(){
    $scope.reserveForm.show();
  };

  $scope.doReserve = function(){
    console.log('Doing reservation',$scope.reservation);

    $timeout(function() {
      $scope.closeReserve();
    }, 1000);

  };


    

})

.controller("MenuController", ['$scope','menuFactory','favoriteFactory', 'baseURL','$ionicListDelegate', function($scope, menuFactory, favoriteFactory,  baseURL, $ionicListDelegate){
         
    $scope.baseURL = baseURL;
    $scope.tab = 1;
    $scope.filtText = '';
    $scope.showDetails = false;
    $scope.showMenu = false;
    $scope.message = "Loading...";
    $scope.dishes = menuFactory.getDishes().query(
      function(reponse){
        $scope.dishes = reponse;
        $scope.showMenu = true;
      },
      function(reponse){
        $scope.message = "Error: "+reponse.status+" "+reponse.statusText;
      }
    );
    
    $scope.select = function(setTab){
      $scope.tab = setTab;

      if(setTab === 2){
        $scope.filtText = "appetizer";
      }
      else if(setTab === 3){
        $scope.filtText = "mains";
      }
      else if(setTab === 4){
        $scope.filtText = "dessert";
      }
      else{
        $scope.filtText = "";
      }
    };

    $scope.isSelected = function(checkTab){
      return ( $scope.tab === checkTab);
    };

    $scope.toggleDetails = function(){
      $scope.showDetails = !$scope.showDetails;
    };

    $scope.addFavorite = function(index){
      console.log("index is "+index);
      favoriteFactory.addToFavorites(index);
      $ionicListDelegate.closeOptionButtons();
    }

  }])

  .controller('ContactController',['$scope',function($scope){

    $scope.feedback = {myChannel:"", firstName:"", lastName:"", agree:false, email:""};

    var channels = [{value:'tel', label:"Tel."},{value:'email', label:'Email'}];
    $scope.channels = channels;
    $scope.invalidChannelSelection = false;

  }])

  .controller('FeedbackController',['$scope', 'feedbackFactory', function($scope, feedbackFactory){

    $scope.sendFeedback = function(){
      console.log($scope.feedback);

      if($scope.feedback.agree && ($scope.feedback.myChannel === "")){
        $scope.invalidChannelSelection = true;
        console.log('incorrect');
      } else {
        feedbackFactory.getFeedback().save($scope.feedback);
        $scope.invalidChannelSelection = false;
        $scope.feedback = {myChannel:"", firstName:"",lastName:"",agree:false,email:"" };
        $scope.feedback.myChannel="";
        $scope.feedbackForm.$setPristine();
        console.log($scope.feedback);
      }
    };

      
  }])

  .controller('DishDetailController', ['$scope', '$stateParams', 'menuFactory', 'favoriteFactory', '$ionicListDelegate', '$ionicPopover',  'baseURL', function($scope, $stateParams, menuFactory,favoriteFactory,$ionicListDelegate, $ionicPopover, baseURL) {
    $scope.baseURL = baseURL;
    $scope.sortCriteria = "";
    $scope.showDishDetail = false;
    $scope.message = "Loading...";
    $scope.dish = menuFactory.getDishes().get({id:parseInt($stateParams.id,10)})
    .$promise.then(
      function(response){
        $scope.dish = response;
        $scope.showDishDetail = true;
      },
      function(response){
        $scope.message = "Error: "+response.status+" "+response.statusText;
      }
    );
    

    $ionicPopover.fromTemplateUrl('dish-detail-popover.html', {
      scope: $scope
    }).then(function(popover){
      $scope.popover = popover;
    });


    $scope.addFavorite = function(){
      console.log("index is "+parseInt($stateParams.id,10));
      favoriteFactory.addToFavorites(parseInt($stateParams.id,10));
      $ionicListDelegate.closeOptionButtons();
    }

    $scope.newComment = {author:"", rating:5, comment:"", date:""};
    $scope.show = true;
        
  }])

  .controller('DishCommentController', ['$scope','menuFactory', function($scope, menuFactory){
    
    $scope.submitComment = function(){
      $scope.newComment.date = new Date().toISOString();
      $scope.dish.comments.push($scope.newComment);
      menuFactory.getDishes().update({id:$scope.dish.id},$scope.dish);
      $scope.dishCommentForm.$setPristine();
      $scope.newComment = {author:"", rating:5, comment:"", date:""};
  
    };

  }])

  .controller('AboutController', ['$scope', 'corporateFactory', 'baseURL', function($scope, corporateFactory, baseURL){

    $scope.baseURL = baseURL;
    $scope.showLeadership = false;
    $scope.leadershipMessage = "Loading...";

    $scope.leaders = corporateFactory.getLeaders().query()
    .$promise.then(
      function(response){
      $scope.leaders = response;
      $scope.showLeadership = true;
    },
    function(response){
      $scope.leadershipMessage = "Error: "+response.status+" "+response.statusText;
    });

  }])


  .controller('FavoritesController',['$scope', 'menuFactory', 'favoriteFactory', 'baseURL', '$ionicListDelegate', 
    function($scope, menuFactory, favoriteFactory, baseURL, $ionicListDelegate){
    $scope.baseURL = baseURL;
    $scope.shouldShowDelete = false;
    
    $scope.favorites = favoriteFactory.getFavorites();

    $scope.dishes = menuFactory.getDishes().query(
      function(reponse){
        $scope.dishes = reponse;
        console.log('Fatched');
      },
      function(reponse){
        $scope.message = "Error: "+reponse.status+" "+reponse.statusText;
      }
    );

     
    

    $scope.toggleDelete = function(){
      $scope.shouldShowDelete = !$scope.shouldShowDelete;
      console.log($scope.shouldShowDelete);
    };

    $scope.deleteFavorite = function(index){
      favoriteFactory.deleteFromFavorites(index);
      $scope.shouldShowDelete = false;
    };

  }])

  .controller('IndexController', ['$scope','menuFactory', 'baseURL', 'corporateFactory', function($scope, menuFactory, baseURL, corporateFactory){

    $scope.baseURL = baseURL;
    $scope.featureDish = {};
    $scope.showFeatureDish = false;
    $scope.featureDishMessage = "Loading...";
    $scope.showPromotion = false;
    $scope.promotionMessage = "Loading...";
    $scope.showLeadership = false;
    $scope.leadershipMessage = "Loading...";
    $scope.featureDish = menuFactory.getDishes().get({id:0})
    .$promise.then(
      function(response){
        $scope.featureDish = response;
        $scope.showFeatureDish = true;

      },
      function(response){
        $scope.featureDishMessage = "Error: "+response.status+" "+response.statusText;
      }
    );

    $scope.promotion = menuFactory.getPromotion().get({id:0})
    .$promise.then(
      function(response){
        $scope.promotion = response;
        $scope.showPromotion = true;
      },
      function(response){
        $scope.promotionMessage = "Error: "+response.status+" "+response.statusText;
      }
    );

    $scope.leader = corporateFactory.getLeaders().get({id:3})
    .$promise.then(
      function(response){
        $scope.leader = response;
        $scope.showLeadership = true;
      },
      function(response){
        $scope.leadershipMessage = "Error: "+response.status+" "+response.statusText;
      }
    );

  }])

  .filter('favoriteFilter', function () {
      return function (dishes, favorites) {
        var out = [];
        for (var i = 0; i < favorites.length; i++) {
          for (var j = 0; j < dishes.length; j++) {
             if (dishes[j].id === favorites[i].id)
              out.push(dishes[j]);
          }
        }

        return out;

    }});



